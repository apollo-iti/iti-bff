package br.com.itau.hackathon.bff.products.Controller

import br.com.itau.hackathon.bff.products.Response.Products
import br.com.itau.hackathon.bff.products.Service.IProductClient
import br.com.itau.hackathon.bff.products.Service.ProductsService
import br.com.zup.beagle.widget.layout.Screen
import br.com.zup.beagle.widget.ui.Text
import br.com.zup.beagle.widget.ui.TextAlignment
import org.springframework.web.bind.annotation.*

@RestController
class ProductsController(var productClient : IProductClient, var productService :ProductsService) {

    @GetMapping("/bproducts/{sku}")
    fun getbProductsBySku(@PathVariable (value = "sku") sku : String) : Products {
        return productClient.getProductBySku(sku)
    }

    @GetMapping("/v1/products/list")
    fun getProducts() : Screen {
        return productService.getProductsList()
    }

}