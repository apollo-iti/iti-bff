package br.com.itau.hackathon.bff

import br.com.zup.beagle.serialization.jackson.BeagleActionSerializer
import br.com.zup.beagle.serialization.jackson.BeagleComponentSerializer
import br.com.zup.beagle.serialization.jackson.BeagleScreenBuilderSerializer
import br.com.zup.beagle.serialization.jackson.BeagleScreenSerializer
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JacksonConfiguration {
    @Bean
    fun objetcMapper() : ObjectMapper {
        val module = SimpleModule()
        module.addSerializer(BeagleComponentSerializer())
        module.addSerializer(BeagleActionSerializer())
        module.addSerializer(BeagleScreenSerializer())
        module.addSerializer(BeagleScreenBuilderSerializer())

        val mapper = jacksonObjectMapper()
        mapper.registerModule(module)
        mapper.setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)

        return mapper
    }
}