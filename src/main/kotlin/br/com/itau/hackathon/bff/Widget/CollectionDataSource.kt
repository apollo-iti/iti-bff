package br.com.itau.hackathon.bff.Widget

import br.com.itau.hackathon.bff.products.Response.Products

data class CollectionDataSource (
    val cards: List<Products>
)