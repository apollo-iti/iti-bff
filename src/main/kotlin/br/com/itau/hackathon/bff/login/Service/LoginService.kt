package br.com.itau.hackathon.bff.login.Impl

import br.com.itau.hackathon.bff.login.Response.DataUser
import br.com.zup.beagle.action.Action
import br.com.zup.beagle.action.Navigate
import br.com.zup.beagle.action.NavigationType
import br.com.zup.beagle.action.ShowNativeDialog
import org.springframework.stereotype.Service

@Service
class LoginService {
     public fun getLoginWidget(user : DataUser) : Action {

            if (user.userName.length <= 3)
                return ShowNativeDialog(
                title = "Erro Login",
                message =  "Invalid User",
                buttonText = "OK")

            return Navigate(
                    type =  NavigationType.ADD_VIEW,
                    path = "/v1/products/list")
            /*
            type =  NavigationType.SWAP_VIEW,
            path = "products/list")*/
    }
}
