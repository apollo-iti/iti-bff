package br.com.itau.hackathon.bff.products.Service

import br.com.itau.hackathon.bff.Widget.CollectionDataSource
import br.com.itau.hackathon.bff.Widget.CollectionWidget
import br.com.itau.hackathon.bff.products.Response.Products
import br.com.zup.beagle.core.Accessibility
import br.com.zup.beagle.core.Appearance
import br.com.zup.beagle.widget.core.Flex
import br.com.zup.beagle.widget.layout.Container
import br.com.zup.beagle.widget.layout.NavigationBar
import br.com.zup.beagle.widget.layout.Screen
import br.com.zup.beagle.widget.ui.Text
import br.com.zup.beagle.widget.ui.TextAlignment
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RestController

@Service
@RestController
class ProductsService (var productClient : IProductClient) {

     fun getProductsList() : Screen {
         return Screen(
            navigationBar = NavigationBar(title = "Coffe"),
            content = Container(
                children = listOf(
                    Text("Here its a custom component in this case a Collection View", alignment = TextAlignment.CENTER),
                    CollectionWidget(dataSource = CollectionDataSource(cards = this.getProducts())
                    ).applyFlex(Flex(grow = 1.0)).applyAppearance(Appearance(backgroundColor = "f3f3f3")).applyAccessibility(Accessibility())
                )
            )
         )
    }

    fun getProducts() : List<Products> {
        val prod = productClient.getProduct()
        return prod
    }
}
