package br.com.itau.hackathon.bff.Widget

import br.com.zup.beagle.annotation.RegisterWidget
import br.com.zup.beagle.core.Accessibility
import br.com.zup.beagle.core.Appearance
import br.com.zup.beagle.widget.Widget
import br.com.zup.beagle.widget.core.Flex

@RegisterWidget
class CollectionWidget (
    val dataSource: CollectionDataSource
) : Widget()