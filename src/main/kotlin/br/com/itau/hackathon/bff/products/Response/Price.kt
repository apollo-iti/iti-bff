package br.com.itau.hackathon.bff.products.Response

data class Price (
        val amount :Double,
        val scale :Int,
        val currencyCode :String
)