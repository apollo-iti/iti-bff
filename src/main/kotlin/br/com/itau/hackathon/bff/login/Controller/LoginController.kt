package br.com.itau.hackathon.bff.login

import br.com.itau.hackathon.bff.login.Impl.LoginService
import br.com.itau.hackathon.bff.login.Response.DataUser
import br.com.zup.beagle.action.Action
import org.springframework.web.bind.annotation.*

@RestController
class LoginController (private val loginService: LoginService){

    @PostMapping("/v1/login")
    fun validateUser(@RequestBody user: DataUser): Action {
        return this.loginService.getLoginWidget(user)
    }
}
