package br.com.itau.hackathon.bff

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
class BffApplication

fun main(args: Array<String>) {
	runApplication<BffApplication>(*args)
}
