package br.com.itau.hackathon.bff.products.Service

import br.com.itau.hackathon.bff.products.Response.Products
import br.com.zup.beagle.action.Action
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod


@FeignClient(name="products", url = "http://192.168.0.111:8000")
interface IProductClient {

    @GetMapping(value = ["/products/list"])
    fun getProduct() : List<Products>

    @GetMapping(value = ["/products/{sku}"])
    fun getProductBySku(@PathVariable(value = "sku") sku : String) : Products
}
