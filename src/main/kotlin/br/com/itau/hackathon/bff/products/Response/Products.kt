package br.com.itau.hackathon.bff.products.Response

data class Products (
        val id :String,
        val sku :String,
        val name :String,
        val shortDescription :String,
        val longDescription :String,
        val imageUrl :String,
        val price :Price
)